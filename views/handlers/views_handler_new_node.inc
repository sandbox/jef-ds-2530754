<?php

/**
 * @file
 * New node link handlers. Insert a new node link inside of an area.
 */
class views_handler_new_node extends views_handler_area {

  public $view;
  function option_definition() {

    $options = parent::option_definition();
    $options['bundle'] = array('default' => '');

    return $options;

  }
  
  function init(&$view, &$options) {
    parent::init($view, $options);
	$this->view = $view;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    $query = db_select('node_type', 'n');
	$query->addField('n', 'type', 't');
	$query->addField('n', 'name', 'na');
    $result = $query->execute();

    $options = $result->fetchAllKeyed();

    $form['bundle'] = array(
      '#type' => 'select',
      '#title' => t('Node type'),
      '#default_value' => $this->options['bundle'],
      '#description' => t("The node type to add a 'new' link for."),
      '#options' => $options,
    );


  }

  /**
   * Render the area
   */
  function render($empty = FALSE) {

    if ((!$empty || !empty($this->options['empty'])) && !empty($this->options['bundle'])) {
	  $bundle = $this->options['bundle'];
	  $display = $this->view->display[$this->view->current_display];
	  kprint_r($display);
      return l(t('Add a new @bundle', array('@bundle' => $bundle)), 'node/add/'.$bundle, array('query' => array('destination' =>$display->display_options['path'])));

    }

    return '';

  }

}