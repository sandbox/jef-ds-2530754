<?php
/**
 * @file
 * Provide views data and handlers for views_new_node.module
 */

/**
 * Implements hook_views_data()
 */
function views_new_node_views_data() {

  $data['views']['new_node'] = array(
    'title' => t('New node link'),
    'help' => t('Adds a new node link inside an area.'),
    'area' => array(
      'handler' => 'views_handler_new_node',
    ),
  );

  return $data;

}